package br.com.alura.convidados.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.alura.convidados.model.Convidado;
import br.com.alura.convidados.service.ConvidadoService;
import br.com.alura.email.service.EmailService;

@Controller
public class ConvidadosController {

	@Autowired
	private ConvidadoService service;

	@RequestMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("novo")
	public String formNovoConvidado(Convidado convidado) {
		return "novoconvidado";
	}

	@GetMapping("listar")
	public String listaConvidados(Model model) {
		model.addAttribute("convidados", service.listar());

		return "listaconvidados";
	}

	@PostMapping("salvar")
	public String adicionaConvidado(@Valid Convidado convidado, BindingResult result, Model model,
			RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			ModelAndView md = new ModelAndView();
			md.addObject(convidado);

			if (convidado.getId() != null) {
				return "editaconvidado";
			} else {
				return "novoconvidado";
			}
		}

		service.salvar(convidado);

		EmailService emailService = new EmailService();
		emailService.enviar(convidado.getNome(), convidado.getEmail());

		redirectAttributes.addFlashAttribute("mensagem", "Convidado salvo com sucesso!");

		return "redirect:listar";
	}

	@GetMapping("editar/{id}")
	public String editaConvidado(@PathVariable("id") Long id, Model model) {
		Convidado convidado = service.procurar(id).get();
		model.addAttribute("convidado", convidado);

		return "editaconvidado";
	}

	@GetMapping("excluir/{id}")
	public String excluiConvidado(@PathVariable("id") Long id, RedirectAttributes redirectAttributes) {
		Convidado convidado = service.procurar(id).get();

		service.excluir(convidado);
		redirectAttributes.addFlashAttribute("mensagem", "Convidado excluído com sucesso!");

		return "redirect:/listar";
	}

}
