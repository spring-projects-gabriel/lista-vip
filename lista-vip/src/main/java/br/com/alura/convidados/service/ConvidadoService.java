package br.com.alura.convidados.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.alura.convidados.model.Convidado;
import br.com.alura.convidados.repository.ConvidadoRepository;

@Service
public class ConvidadoService {

	@Autowired
	private ConvidadoRepository repository;

	public List<Convidado> listar() {
		return repository.findAll();
	}

	public void salvar(Convidado convidado) {
		repository.save(convidado);
	}

	public Optional<Convidado> procurar(Long id) {
		Optional<Convidado> convidado = repository.findById(id);
		return convidado;
	}

	public void excluir(Convidado convidado) {
		repository.delete(convidado);
	}

}
