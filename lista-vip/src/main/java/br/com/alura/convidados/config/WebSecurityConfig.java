package br.com.alura.convidados.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests() 
		.antMatchers("/h2-console/**").permitAll()
		//.antMatchers(HttpMethod.GET, "/").permitAll()
		.anyRequest().authenticated()
		.and().headers().frameOptions().disable()
		.and().formLogin().loginPage("/login") // Se ele não estiver autenticado é direcionado para o formulário de login
		.defaultSuccessUrl("/").permitAll()
		.and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).permitAll();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder autorizacao) throws Exception {
		/* Antes do Spring Security 5.0, o padrão PasswordEncoder era NoopPasswordEncoder, ou seja, quais senhas de texto simples requeriam. No Spring Security 5, o padrão é 
		   DelegatingPasswordEncoder, o que exigiu o formato de armazenamento de senha. Por isso, adicionei o formato de armazenamento de senha para texto simples {noop}
		*/
		autorizacao.inMemoryAuthentication()
		.withUser("user").password("{noop}123").roles("USER")
		.and()
	    .withUser("admin").password("{noop}456").roles("ADMIN");
	}
	
    // Forma recomendada de ignorar no filtro de segurança as requisições para recursos estáticos
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/bootstrap/**");
    }
	
}
