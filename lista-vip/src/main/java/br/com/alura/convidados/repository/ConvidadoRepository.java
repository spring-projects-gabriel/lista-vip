package br.com.alura.convidados.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.alura.convidados.model.Convidado;

public interface ConvidadoRepository extends JpaRepository<Convidado, Long> {

}
