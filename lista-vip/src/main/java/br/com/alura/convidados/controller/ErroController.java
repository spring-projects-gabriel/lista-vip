package br.com.alura.convidados.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErroController implements ErrorController {

	// Crio um mapeamento para o caminho retornado pelo getErrorPath(). Desta forma, o controlador pode manipular chamadas para o  caminho /error .
	@Override
	public String getErrorPath() {
		return "/erro";
	}

	// No handleError(), retorno a página de erro personalizada que criei. Se disparar um erro 404 agora, é a página personalizada que será exibida.
	@RequestMapping("/error")
	public String handleError() {
		return "erro";
	}

}
